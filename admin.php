<?php
include ('config.php');
include ('grabber.php');
$db = new PDO($database);

if (!empty($_POST['action'])) {
    switch ($_POST['action']) {
        case 'add':
            $_POST['url'] = explode('/', $_POST['url']);

            if (end($_POST['url']) == '') {
                array_pop($_POST['url']);
            }

            if (end($_POST['url']) == 'list') {
                array_pop($_POST['url']);
            }
            $name   = end($_POST['url']);
            $url    = 'http://www.biglion.ru/' . $name . '/list/';
            $file   = 'feeds/' . $name . '.xml';

            $feedExists = $db->exec('SELECT name FROM offers WHERE name = ' . $name);
            if (!$feedExists) {
                $db->prepare('INSERT INTO offers (name, url) VALUES (?, ?)')->execute(array($name, $url));
            }

            file_put_contents($file, makeRss($url, $feedParams));
            chmod($file, 0777);

            header('Location: admin.php');
            die();
            break;

        case 'delete':
            if (!empty($_POST['id']) && ctype_digit($_POST['id'])) {
                $statement = $db->prepare('SELECT name FROM offers WHERE id = ?');
                $statement->execute(array($_POST['id']));
                $file = $statement->fetch();

                if (!empty($file['name'])) {
                    $file = 'feeds/' . $file['name'] . '.xml';
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }
                $db->prepare('DELETE FROM offers WHERE id = ?')->execute(array($_POST['id']));
            }

            header('Location: admin.php');
            die();
            break;
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Грабилка</title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<!--        <link rel="stylesheet" type="text/css" href="styles/styles.css" />-->
    </head>
    <body>
        <div id="main">
<?php
if (empty($_GET['page'])) {
    $_GET['page'] = 'list';
}

switch ($_GET['page']) {
    case 'add':
        echo '            <h1>Добавить ленту</h1>
            <form action="admin.php" method="post">
                <label> Адрес страницы с акциями
                    <input type="text" name="url" value="" />
                </label>
                <button name="action" value="add">Добавить</button>
            </form>
            <p><a href="admin.php">Вернуться в список лент</a></p>';
        break;

    case 'list':
    default:
        echo '            <h1>Список лент</h1>
            <p><a href="admin.php?page=add">Добавить ленту</a></p>';
        $items = $db->query('SELECT * FROM offers');
        $feeds = array();
        foreach ($items as $item) {
            $feeds[] = $item;
        }

        if (!empty($feeds)) {
            echo '
            <table class="items" width="100%">
                <tr>
                    <th>Адрес</th>
                    <th>Файл</th>
                    <th>Удалить</th>
                </tr>';

            foreach ($feeds as $item) {
                $file = 'feeds/' . $item['name'] . '.xml';
                echo '
                <tr>
                    <td><a href="' . $item['url'] . '">' . $item['url'] . '</a></td>
                    <td align="center">' . (file_exists($file) ? '<a target="_blank" href="' . $file . '">' . $file . '</a>' : '<b>Отсутствует</b>') . '</td>
                    <td align="center">
                        <form action="admin.php" method="post">
                            <input type="hidden" name="id" value="' . $item['id'] . '"/>
                            <button name="action" value="delete" onclick="return confirm(\'Точно удалить?\')">Удалить</button>
                        </form>
                    </td>
                </tr>';
            }
            unset($item);

            echo '
            </table>';

        } else {
            echo '<p>Лент нет.</p>';
        }
        break;
}
?>

        </div>
    </body>
</html>
