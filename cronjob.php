<?php
set_time_limit(600);
include ('config.php');
include ('grabber.php');
$db = new PDO($database);

$items = $db->query('SELECT name, url FROM offers');

foreach ($items as $item) {
    $feed = makeRss($item['url'], $feedParams);
    if (!empty($feed)) {
        $file = dirname(__FILE__) . '/feeds/' . $item['name'] . '.xml';
        file_put_contents($file, $feed);
        @chmod(dirname(__FILE__) . '/feeds/' . $item['name'] . '.xml', 0777);
        unset($feed);
    }
}
