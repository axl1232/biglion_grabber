<?php
function makeRss($page, $feedParams) {
    $data   = file_get_contents($page);
    $log    = array();

    if (empty($data)) {
        $log[] = 'Не удалось получить содержимое ' . $page;

    } else {
        $log[] = 'Получено содержимое ' . $page;
        preg_match('/<!-- Recent offers -->(.*)<!-- \/Recent offers -->/is', $data, $offers);
        unset($data);

        if (empty($offers)) {
            $log[] = 'Не удалось получить список акций';

        } else {
            $log[]  = 'Получен список акций';
            $offers = str_replace(chr(13), '', $offers[1]);
            //для исключения варнингов о повторяющемся атрибуте id
            $offers = str_replace('id=', 'cid=', $offers);

            $offers = '<meta http-equiv="Content-type" content="text/html; charset=UTF-8">' . $offers;  //хак для правильного определения кодировки фрагмента
            $xml = new DOMDocument;
            $offers = str_replace('&nbsp;', ' ', $offers);
            $offers = str_replace('&', '&amp;', $offers);
            $xml->loadHTML($offers);

            $xpath = new DOMXPath($xml);
            $divs = $xpath->evaluate('/html/body/div/div');

            foreach ($divs as $div) {
                $actionUrl = '';
                $tempFile = dirname(__FILE__) . '/tempImage';
                $imageUrl = $xpath->evaluate('div/div/a/img', $div)->item(0)->getAttribute('src');

                //получаем информацию об изображении акции
                $image = file_get_contents($imageUrl);
                file_put_contents($tempFile, $image);
                chmod($tempFile, 0777);

                $imageData = getimagesize($tempFile);
                $imageData = array(
                    'size' => filesize($tempFile),
                    'type' => $imageData['mime'],
                    'path' => strtr($imageUrl, array('[' => '%5B', ']' => '%5D'))
                );
                unlink($tempFile);
                unset($image);

                $imageDataNode = $xml->createElement('span');
                $imageDataNode->setAttribute('id', 'imageData');
                $imageDataNode->setAttribute('size', $imageData['size']);
                $imageDataNode->setAttribute('type', $imageData['type']);
                $imageDataNode->setAttribute('path', $imageData['path']);
                $div->appendChild($imageDataNode);

                //получаем ссылку на акцию
                $link = $xpath->evaluate('div/h4/a/@href', $div)->item(0)->value;
                $linkNode = $xml->createElement('span', $link . $feedParams['linkSuffix']);
                $linkNode->setAttribute('id', 'offerLink');
                $div->appendChild($linkNode);

                //получаем условия акции
                $offerPageUrl = $xpath->evaluate('div/div/a/@href', $div)->item(0)->value;
                $offerPage = file_get_contents($offerPageUrl);
                preg_match('/<!-- Offer decription -->(.*)<!-- \/Offer decription -->/is', $offerPage, $offerPage);
                $offerPage = $offerPage[1];
                $offerPage = preg_replace('/<a.*?\/a>/is', '', $offerPage);
                $offerPage = preg_replace('/<style.*?\/style>/is', '', $offerPage);
                $offerPage = trim(strip_tags($offerPage, '<h2><ul><li><br><strong><p>'));
                $offerPage = str_replace(chr(13), '', $offerPage);
                $offerPage = str_replace('&nbsp;', ' ', $offerPage);
                $offerPage = str_replace('&', '&amp;', $offerPage);

                //получаем дату окончания акции
                preg_match('/<li>Купон действует до (\d+)\.(\d+)\.(\d+)<\/li>/', $offerPage, $offerDate);
                $offerDate = date('r', mktime(0, 0, 1, $offerDate[2], $offerDate[1], $offerDate[3]));

                $offerDescriptionNode = $xml->createElement('span');
                $offerDescriptionNode->appendChild($xml->createCDATASection($offerPage));
                $offerDescriptionNode->setAttribute('id', 'offerDescription');
                $div->appendChild($offerDescriptionNode);

                $offerDateNode = $xml->createElement('span', $offerDate);
                $offerDateNode->setAttribute('id', 'offerDate');
                $div->appendChild($offerDateNode);
            }

            $xsl = new DOMDocument;
            $xsl->load('offers.xsl');

            $proc = new XSLTProcessor;
            $proc->importStyleSheet($xsl);
            $proc->setParameter('', $feedParams);
//var_dump($xml->saveXML());
            return $proc->transformToXml($xml);
        }
    }
}
