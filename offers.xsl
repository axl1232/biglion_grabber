<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>

    <xsl:template match="/">
        <rss version="2.0">
            <channel>
                <title><xsl:value-of select="$feedTitle"/></title>
                <link><xsl:value-of select="$feedLink"/></link>
                <description><xsl:value-of select="$feedDescription"/></description>
                <lastBuildDate><xsl:value-of select="$lastBuildDate"/></lastBuildDate>
                <ttl>6</ttl>
                <xsl:apply-templates select="html/body/div/div"/>
            </channel>
        </rss>
    </xsl:template>

    <xsl:template match="html/body/div/div">
        <item>
            <xsl:apply-templates select="div"/>
        </item>
    </xsl:template>

    <xsl:template match="div">
        <title><xsl:value-of select="div/a/img/@title"/></title>
        <link><xsl:value-of select="../span[@id='offerLink']/text()"/></link>
        <description><xsl:value-of select="../span[@id='offerDescription']/text()"/></description>
        <enclosure url="{../span[@id='imageData']/@path}" type="{../span[@id='imageData']/@type}" length="{../span[@id='imageData']/@size}" />
        <pubDate><xsl:value-of select="../span[@id='offerDate']/text()" /></pubDate>
        <guid><xsl:value-of select="../span[@id='offerLink']/text()"/></guid>
    </xsl:template>

</xsl:stylesheet>
